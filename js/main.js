$(function() {
  $('.menu-mobile').click(function(){
    $('.bg-menu').css('display', 'block');
    $('.bg-menu, .mfp-close').css('opacity', 1);
    $('.sidebar').css('right', 0);
    $('.mfp-close').css('top', '15px');
    
  });
  
  $('.mfp-close').click(function(){
    $('.mfp-close').css('top', '-50px');
    $('.bg-menu').css('display', 'none');
    $('.bg-menu, .mfp-close').css('opacity', 0);
    $('.sidebar').css('right', '-260px');
  });

  $('.area-popup p').click(function(){
    var form = $(this).data("form");
    $(".option-input"+form).empty();
    $(this).children().clone().appendTo(".option-input"+form);
    $('.value'+form).val($(this).data("id"));
  });

  $('.item-money').click(function(){
    $(".item-money").removeClass('active');
    $(this).addClass('active');
  });

  $('.status-profile a').click(function(){
    var form = $(this).data("type");
    $('.'+form).select();
  });

  $('.item-tree').click(function(){
    $(this).parent().addClass('hasMore');
  });

  $('.close-tree span').click(function(){
    $(this).parent().parent().parent().removeClass('hasMore');
  });

  $(document).mouseup(function(e){
    var container = $(".option-input");
    $(".area-popup").css('display', 'block');
    if(!container.is(e.target) && container.has(e.target).length === 0){
      $(".area-popup").css('display', 'none');
    }
  });
  
});